package com.example.demo.logincheck;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.core.Is.is;


public class LoginPasswordValidatorTest {
    //Возможно сделать параметризованный тест было бы лучше
    private static final String[][] INPUT = new String[][]{{null,null}, {"",""},{"login","password"},{"  ","  "}};
    private static final Boolean[] EXPECTED = new Boolean[]{false,false,true,false};

    @Test
    public void isValid() {
        for(int i = 0;i<INPUT.length-1;i++){
            Assert.assertThat(LoginPasswordValidator.isValid(INPUT[i][0],INPUT[i][1]), is(EXPECTED[i]));
        }
    }
}