package com.example.demo.logincheck;

class LoginPasswordValidator {


    static boolean isValid(String login, String password) {
        if (login != null && !login.trim().isEmpty()) {
            return password != null && !password.trim().isEmpty();
        }
        return false;
    }

}

