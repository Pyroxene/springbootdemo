package com.example.demo;

import com.example.demo.model.Booking;
import com.example.demo.model.Item;
import com.example.demo.model.User;
import com.example.demo.repository.ItemRepository;
import com.example.demo.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@Slf4j
@Component
public class PreLoadData implements ApplicationRunner {
    private UserRepository userRepository;
    private ItemRepository itemRepository;

    //    @Autowired
    public PreLoadData(UserRepository userRepository, ItemRepository itemRepository) {
        this.userRepository = userRepository;
        this.itemRepository = itemRepository;
    }

    public void run(ApplicationArguments args) {
        List<Item> items = Stream.of("IceCream", "Car", "Hexagon", "Bodypart")
                .map(Item::new)
                .collect(Collectors.toList());
        Iterable<Item> savedItems = itemRepository.saveAll(items);


        User user = new User("Oleg", "Manilov");
        Booking booking = new Booking();
        booking.setUser(user);
        user.getBookings()
                .add(booking);

        savedItems.forEach(booking.getItems()::add);
        User savedUser = userRepository.save(user);

    }
}
