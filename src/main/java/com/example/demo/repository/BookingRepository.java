package com.example.demo.repository;

import com.example.demo.model.Booking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookingRepository extends CrudRepository<Booking, Long> {
    Optional<Booking> findById (Long id);
    Booking findBookingById (Long id);
    Booking findBookingByIdAndUserId(Long user_id, Long booking_id);
    List<Booking> findBookingsByUserId (Long id);
}
