package com.example.demo.controller;

import com.example.demo.model.Booking;
import com.example.demo.service.BookingService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class BookingController {

    private final BookingService bookingService;

    @GetMapping("/users/{id}/bookings")
    public List<Booking> getAllBookings(@PathVariable Long id){
        return bookingService.findAll(id);
    }

    @RequestMapping("/users/{user_id}/bookings/{id}")
    public Booking getBooking(@PathVariable long user_id, @PathVariable long id){
        return bookingService.getById(user_id, id);
    }

}
