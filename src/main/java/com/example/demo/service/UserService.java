package com.example.demo.service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;


    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }


    public User getUser(Long id) {
        return userRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    public void addUser(User user) {
        userRepository.save(user);
    }

    public void updateUser(User user){
        if(!userRepository.existsById(user.getId())){
            throw new RuntimeException();
        }

        userRepository.save(user);
    }

    public void deleteUser(Long id){
        if(!userRepository.existsById(id)){
            throw new RuntimeException();
        }

        userRepository.deleteById(id);
    }
}
