package com.example.demo.service;

import com.example.demo.model.Booking;
import com.example.demo.repository.BookingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BookingService {

    private final BookingRepository bookingRepository;

    public List<Booking> findAll(Long id) {
        return bookingRepository.findBookingsByUserId(id);
    }


    public Booking getById(long user_id, long id) {
        return bookingRepository.findBookingByIdAndUserId(user_id, id);
    }

}
