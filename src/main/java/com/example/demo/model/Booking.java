package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Data
@Entity
@ToString(exclude = "user")
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
    private User user;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "booking_item_ids", joinColumns = @JoinColumn(name = "booking_id"))
    @Column(name = "item_id")
    private List<Item> items = new LinkedList<>();
}