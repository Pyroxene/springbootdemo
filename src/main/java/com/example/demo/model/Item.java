package com.example.demo.model;

import lombok.*;

import javax.persistence.*;


@Entity
@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private String itemName;

    public Item(String itemName) {
        this.itemName = itemName;
    }
}
